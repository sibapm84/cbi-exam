variable "cidr_block" {
  description = "VPC CIDR block"
  default     = "172.27.0.0/16"
}

variable "vpc_name" {
  description = "VPC Name"
  default     = "account1"
}

variable account_name {
  description = "Environment Name"

  default     = ""
}

# variable "env" {
#   description = "Environment Name"

# }



variable "availability_zones" {
  description = "A list of AZs to be included in VPC"
  
}

variable "public_subnets" {
  description = "A list all the public subnets in the VPC"
  
}

variable "private_subnets" {
  description = "A list all the private subnets in the VPC"
  
}

variable "tags" {
  description = "Tags for VPC"
  default     = {}
}

variable "enable_dns_support" {
  description = "Enable/Disable DNS support in the VPC"
 
}

variable "enable_dns_hostnames" {
  description = "Enable/Disable DNS hostname in the VPC"
 
}

variable "enable_nat_gateway" {
  description = "Enable/Disable nat gateway in public subnets"
  default = true

}