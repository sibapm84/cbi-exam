output "vpc_id" {
  description = "VPC Created ID"
  value       = aws_vpc.vpc.id
}

output "vpc_owner" {
  description = "VPC Owner ID"
  value = aws_vpc.vpc.owner_id
  }

output "cidr_block" {
  description = "VPC CIDR block"
  value       = aws_vpc.vpc.cidr_block
}

output "public_subnet_ids" {
  description = "VPC public subnet ids"
  value       = aws_subnet.public.*.id
}

output "private_subnets_ids" {
  description = "VPC private subnet ids"
  value       = aws_subnet.private.*.id
}

