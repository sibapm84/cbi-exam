variable "region" {
  default = "us-east-1"
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
  profile = "local"
}

module "vpcstg1" {
  source = "../../../modules/vpc/"
  # env = "stage1"
  cidr_block = "192.168.0.0/16"
  account_name = "account1"
  enable_dns_support   = true
  enable_dns_hostnames = true
  availability_zones =  ["us-east-1a", "us-east-1b"]
  public_subnets = ["192.168.21.0/24", "192.168.22.0/24"]
  private_subnets = ["192.168.31.0/24", "192.168.32.0/24"]
  
  tags = {
    Environment = "Stage1"
    Terraform   = "True"
    Account        = "Account1"
  }
}


provider "aws" {
  alias = "stage2"
  region  = "us-east-1"
  version = "~> 2.54.0"
  profile = "stage2"
}


module "vpcstg2" {
  source = "../../../modules/vpc/"
  providers = {
    aws = aws.stage2
  }
  cidr_block = "195.168.0.0/16"
  account_name = "account2"
  enable_dns_support   = true
  enable_dns_hostnames = true
  availability_zones =  ["us-east-1a", "us-east-1b"]
  public_subnets = ["195.168.21.0/24", "195.168.22.0/24"]
  private_subnets = ["195.168.31.0/24", "195.168.32.0/24"]
  
  tags = {
    Environment = "Stage2"
    Terraform   = "True"
    Account        = "Account2"
  }
}

data "aws_caller_identity" "stg2" {
  provider = aws.stage2
}

data "aws_region" "stg2" {
  provider = aws.stage2
}

resource "aws_vpc_peering_connection" "stg2" {
  vpc_id        = module.vpcstg1.vpc_id
  peer_vpc_id   = module.vpcstg2.vpc_id
  peer_owner_id = data.aws_caller_identity.stg2.account_id
  peer_region   = data.aws_region.stg2.id
  auto_accept   = false
  tags = {
      Name = "stg2-peering-stg1"
    }
}


resource "aws_vpc_peering_connection_accepter" "stg1" {
  vpc_peering_connection_id = aws_vpc_peering_connection.stg2.id
  auto_accept               = true
  tags = {
      Name = "stg2-peering-stg1"
    }
}

# resource "aws_route" "pub_route" {
#   route_table_id            = module.vpcstg1.public_route_table_id
#   destination_cidr_block    = module.vpcstg1.cidr_block
#   vpc_peering_connection_id = aws_vpc_peering_connection.stg2.id
#   depends_on                = [aws_vpc_peering_connection.stg2]
# }


# resource "aws_route" "pri_route" {
#   route_table_id            = module.vpcstg1.private_route_table_id
#   destination_cidr_block    = module.vpcstg1.cidr_block
#   vpc_peering_connection_id = aws_vpc_peering_connection.stg2.id
#   depends_on                = [aws_vpc_peering_connection.stg2]
# }


# resource "aws_route" "pub_route2" {
#   route_table_id            = module.vpcstg2.public_route_table_id2
#   destination_cidr_block    =  module.vpcstg2.cidr_block
#   vpc_peering_connection_id = aws_vpc_peering_connection.stg2.id
#   depends_on                = [aws_vpc_peering_connection.stg2]
# }


# resource "aws_route" "pri_route2" {
#   route_table_id            =  module.vpcstg2.private_route_table_id2
#   destination_cidr_block    =  module.vpcstg2.cidr_block
#   vpc_peering_connection_id = aws_vpc_peering_connection.stg2.id
#   depends_on                = [aws_vpc_peering_connection.stg2]
# }