output "vpc_id" {
  value = module.vpcstg1.vpc_id
}

output "cidr_block" {
  value = module.vpcstg1.cidr_block
}

output "account_owner" {
  value = module.vpcstg1.vpc_owner
}

output "public_subnet_ids" {
  value = module.vpcstg1.public_subnet_ids
}

output "private_subnets_ids" {
  value = module.vpcstg1.private_subnets_ids
}

output "public_route_table_id" {
  value = module.vpcstg1.public_route_table_id
}

output "private_route_table_id" {
  value = module.vpcstg1.private_route_table_id
}



output "vpc_id2" {
  value = module.vpcstg2.vpc_id
}

output "cidr_block2" {
  value = module.vpcstg2.cidr_block
}

output "account_owner2" {
  value = module.vpcstg2.vpc_owner
}

output "public_subnet_ids2" {
  value = module.vpcstg2.public_subnet_ids
}

output "private_subnets_ids2" {
  value = module.vpcstg2.private_subnets_ids
}

output "public_route_table_id2" {
  value = module.vpcstg2.public_route_table_id
}

output "private_route_table_id2" {
  value = module.vpcstg2.private_route_table_id
}