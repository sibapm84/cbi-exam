Apply complete! Resources: 0 added, 0 changed, 2 destroyed.

Outputs:

account_owner = 747174544485
account_owner2 = 747174544485
cidr_block = 192.168.0.0/16
cidr_block2 = 195.168.0.0/16
private_route_table_id = rtb-008a7ba6172b3fbd8
private_route_table_id2 = rtb-038ac33f81316ffc7
private_subnets_ids = [
  "subnet-0cb22c0cb9a40d9da",
  "subnet-069b6955f803c9915",
]
private_subnets_ids2 = [
  "subnet-0de459f707082178b",
  "subnet-07b25a0cce4e62b39",
]
public_route_table_id = rtb-0fce32fbb9bdbf6d5
public_route_table_id2 = rtb-05873351f20d58f59
public_subnet_ids = [
  "subnet-00f10120c8b9dc583",
  "subnet-05e021dd6fd22f046",
]
public_subnet_ids2 = [
  "subnet-0d3d8e7c743838e31",
  "subnet-08242505e4c281ed1",
]
vpc_id = vpc-033c6a8bfa8d94a60
vpc_id2 = vpc-00899c454b8db6273
