# variable "region" {
#   default = "us-east-1"
# }

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.55.0"
}


module "vpcstg2" {
  source = "../../../modules/vpc/"
  cidr_block = "195.168.0.0/16"
  account_name = "account2"
  enable_dns_support   = true
  enable_dns_hostnames = true
  availability_zones =  ["ap-south-1a", "ap-south-1b"]
  public_subnets = ["195.168.21.0/24", "195.168.22.0/24"]
  private_subnets = ["195.168.31.0/24", "195.168.32.0/24"]
  
  tags = {
    Environment = "Stage2"
    Terraform   = "True"
    Account        = "Account2"
  }
}