output "vpc_id" {
  value = module.vpcstg2.vpc_id
}

output "cidr_block" {
  value = module.vpcstg2.cidr_block
}

output "account_owner" {
  value = module.vpcstg2.vpc_owner
}

output "public_subnet_ids" {
  value = module.vpcstg2.public_subnet_ids
}

output "private_subnets_ids" {
  value = module.vpcstg2.private_subnets_ids
}
